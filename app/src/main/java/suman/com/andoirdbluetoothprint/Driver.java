package suman.com.andoirdbluetoothprint;

import java.io.IOException;
import java.io.OutputStream;

public class Driver {

    //ESC/POS CONSTANTS
//    public static final byte CMD_LF = 0x0A;
//    public static final byte CMD_FS = 0x1C;
//    public static final byte CMD_FF = 0x0C;
//    public static final byte CMD_GS = 0x1D;
//    public static final byte CMD_DLE = 0x10;
//    public static final byte CMD_EOT = 0x04;
//    public static final byte CMD_NUL = 0x00;
//    public static final byte CMD_ESC = 0x1B;
//    public static final byte CMD_EOL = "\n";

    //FEED_CONTROL_SEQUENCES
    public static final byte CTL_LF = 0x0A; // Prbyte and line feed
    public static final byte CTL_FF = 0x0C; // Form feed
    public static final byte CTL_CR = 0x0D; // Carriage return
    public static final byte CTL_HT = 0x09; // Horizontal tab
    public static final byte CTL_VT = 0x0B; // Vertical tab

    //LINE SPACING
    public static final byte LS_DEFAULT[] = {0x1B, 0x32}; // Vertical tab
    public static final byte LS_SET[] = {0x1B, 0x33}; // Vertical tab

    //HARDWARE
    public static final byte HW_INIT[] = {0x1B, 0x40}; // Clear data in buffer and reset modes
    public static final byte HW_SELECT[] = {0x1B, 0x3D, 0x01}; // Prbyteer select
    public static final byte HW_RESET[] = {0x1B, 0x3F, 0x0A, 0x00}; // Reset prbyteer hardware

    //CASH DRAWER
    public static final byte CD_KICK_2[] = {0x1B, 0x70, 0x00};  // Sends a pulse to pin 2 []
    public static final byte CD_KICK_5[] = {0x1B, 0x70, 0x01};  // Sends a pulse to pin 5 []

    //CUT PAPER
    /*
_.PAPER = {
  PAPER_FULL_CUT  : '\x1d\x56\x00' , // Full cut paper
  PAPER_PART_CUT  : '\x1d\x56\x01' , // Partial cut paper
  PAPER_CUT_A     : '\x1d\x56\x41' , // Partial cut paper
  PAPER_CUT_B     : '\x1d\x56\x42' , // Partial cut paper
};
 */

    //TEXT FORMAT
    public static final byte TXT_NORMAL[] =  {0x1B, 0x21, 0x00};// Normal text
    public static final byte TXT_2HEIGHT[] = {0x1B, 0x21, 0x10}; // Double height text
    public static final byte TXT_2WIDTH[] =  {0x1B, 0x21, 0x20}; // Double width text

    public static final byte TXT_UNDERL_OFF[] =  {0x1B, 0x2D, 0x00};// Underline font OFF
    public static final byte TXT_UNDERL_ON[] =   {0x1B, 0x2D, 0x01}; // Underline font 1-dot ON
    public static final byte TXT_UNDERL2_ON[] =  {0x1B, 0x2D, 0x02}; // Underline font 2-dot ON

    public static final byte TXT_EMPH_OFF[] =   {0x1B, 0x45, 0x00}; // Underline font 1-dot ON
    public static final byte TXT_EMPH_ON[] =  {0x1B, 0x21, 0x01}; //JUST DO TXT_NORMAL FOR EMPH OFF

    public static final byte TXT_BOLD_OFF[] =    {0x1B, 0x45, 0x00}; // Bold font OFF
    public static final byte TXT_BOLD_ON[] =     {0x1B, 0x45, 0x01};  // Bold font ON

    public static final byte TXT_FONT_A[] = {0x1B, 0x4D, 0x00}; // Font type A
    public static final byte TXT_FONT_B[] = {0x1B, 0x4D, 0x01}; // Font type B
    public static final byte TXT_FONT_C[] = {0x1B, 0x4D, 0x02}; // Font type C
//    public static final byte TXT_FONT_D[] = {0x1B, 0x4D, 0x03}; // Font type D //SAME AS B
//    public static final byte TXT_FONT_E[] = {0x1B, 0x4D, 0x04}; // Font type E //SAME AS B
//    public static final byte TXT_FONT_A2[] = {0x1B, 0x4D, 0x61}; // Special Font type A //SAME AS B
//    public static final byte TXT_FONT_B2[] = {0x1B, 0x4D, 0x62}; // Special Font type B //SAME AS B

    public static final byte TXT_ALIGN_LT[] = {0x1B, 0x61, 0x00}; // Left justification
    public static final byte TXT_ALIGN_CT[] = {0x1B, 0x61, 0x01}; // Centering
    public static final byte TXT_ALIGN_RT[] = {0x1B, 0x61, 0x02}; // Right justification

    //TODO: IMPLEMENT BARCODE/IMAGE/2D CODE

/*
    _.BARCODE_FORMAT = {
        BARCODE_TXT_OFF : '\x1d\x48\x00' , // HRI barcode chars OFF
                BARCODE_TXT_ABV : '\x1d\x48\x01' , // HRI barcode chars above
                BARCODE_TXT_BLW : '\x1d\x48\x02' , // HRI barcode chars below
                BARCODE_TXT_BTH : '\x1d\x48\x03' , // HRI barcode chars both above and below

                BARCODE_FONT_A  : '\x1d\x66\x00' , // Font type A for HRI barcode chars
                BARCODE_FONT_B  : '\x1d\x66\x01' , // Font type B for HRI barcode chars

                BARCODE_HEIGHT  : '\x1d\x68\x64' , // Barcode Height [1-255]
                BARCODE_WIDTH   : '\x1d\x77\x03' , // Barcode Width  [2-6]

                BARCODE_UPC_A   : '\x1d\x6b\x00' , // Barcode type UPC-A
                BARCODE_UPC_E   : '\x1d\x6b\x01' , // Barcode type UPC-E
                BARCODE_EAN13   : '\x1d\x6b\x02' , // Barcode type EAN13
                BARCODE_EAN8    : '\x1d\x6b\x03' , // Barcode type EAN8
                BARCODE_CODE39  : '\x1d\x6b\x04' , // Barcode type CODE39
                BARCODE_ITF     : '\x1d\x6b\x05' , // Barcode type ITF
                BARCODE_NW7     : '\x1d\x6b\x06' , // Barcode type NW7
                BARCODE_CODE93  : '\x1d\x6b\x07' , // Barcode type CODE93
                BARCODE_CODE128 : '\x1d\x6b\x08' , // Barcode type CODE128
    };


    _.CODE2D_FORMAT = {
        TYPE_PDF417     : _.GS  + 'Z' + '\x00',
                TYPE_DATAMATRIX : _.GS  + 'Z' + '\x01',
                TYPE_QR         : _.GS  + 'Z' + '\x02',
                CODE2D          : _.ESC + 'Z'         ,
                QR_LEVEL_L      : 'L', // correct level 7%
                QR_LEVEL_M      : 'M', // correct level 15%
                QR_LEVEL_Q      : 'Q', // correct level 25%
                QR_LEVEL_H      : 'H'  // correct level 30%
    };


    _.IMAGE_FORMAT = {
        S_RASTER_N      : '\x1d\x76\x30\x00' , // Set raster image normal size
                S_RASTER_2W     : '\x1d\x76\x30\x01' , // Set raster image double width
                S_RASTER_2H     : '\x1d\x76\x30\x02' , // Set raster image double height
                S_RASTER_Q      : '\x1d\x76\x30\x03' , // Set raster image quadruple
    };


    _.BITMAP_FORMAT = {
        BITMAP_S8     : '\x1b\x2a\x00',
                BITMAP_D8     : '\x1b\x2a\x01',
                BITMAP_S24    : '\x1b\x2a\x20',
                BITMAP_D24    : '\x1b\x2a\x21'
    };

    _.GSV0_FORMAT = {
        GSV0_NORMAL   : '\x1d\x76\x30\x00',
                GSV0_DW       : '\x1d\x76\x30\x01',
                GSV0_DH       : '\x1d\x76\x30\x02',
                GSV0_DWDH     : '\x1d\x76\x30\x03'
    };
    */

    OutputStream os=null;

    public Driver(OutputStream received_OS){
        os = received_OS;
    }

    public boolean send(String text){
        try {
            os.write(text.getBytes());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    public boolean send (byte bytes[]){
        try {
            os.write(bytes);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }


}
